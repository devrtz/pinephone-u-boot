From b17c3fe41cbe222edd762501b5ebcdf3e20b0f82 Mon Sep 17 00:00:00 2001
From: Samuel Holland <samuel@sholland.org>
Date: Sun, 17 May 2020 21:10:29 -0500
Subject: [PATCH 11/20] sunxi: board: Add a helper to get the SPL DT name

This moves the validity checking and typecasts all to one place away
from the string comparison logic, and it detangles the compile-time
and runtime control flow.

The new helper will also be used by U-Boot proper in a future commit.

Signed-off-by: Samuel Holland <samuel@sholland.org>
---
 board/sunxi/board.c | 26 +++++++++++++++++---------
 1 file changed, 17 insertions(+), 9 deletions(-)

diff --git a/board/sunxi/board.c b/board/sunxi/board.c
index f8b6c86d8f..4b7b343255 100644
--- a/board/sunxi/board.c
+++ b/board/sunxi/board.c
@@ -316,6 +316,17 @@ static struct boot_file_head * get_spl_header(uint8_t req_version)
 	return spl;
 }
 
+static const char *get_spl_dt_name(void)
+{
+	struct boot_file_head *spl = get_spl_header(SPL_DT_HEADER_VERSION);
+
+	/* Check if there is a DT name stored in the SPL header. */
+	if (spl != INVALID_SPL_HEADER && spl->dt_name_offset)
+		return (char *)spl + spl->dt_name_offset;
+
+	return NULL;
+}
+
 int dram_init(void)
 {
 	struct boot_file_head *spl = get_spl_header(SPL_DRAM_HEADER_VERSION);
@@ -888,20 +899,17 @@ int ft_board_setup(void *blob, bd_t *bd)
 #ifdef CONFIG_SPL_LOAD_FIT
 int board_fit_config_name_match(const char *name)
 {
-	struct boot_file_head *spl = get_spl_header(SPL_DT_HEADER_VERSION);
-	const char *best_dt_name = (const char *)spl;
+	const char *best_dt_name = get_spl_dt_name();
 
-	/* Check if there is a DT name stored in the SPL header and use that. */
-	if (spl != INVALID_SPL_HEADER && spl->dt_name_offset) {
-		best_dt_name += spl->dt_name_offset;
-	} else {
 #ifdef CONFIG_DEFAULT_DEVICE_TREE
+	if (best_dt_name == NULL)
 		best_dt_name = CONFIG_DEFAULT_DEVICE_TREE;
-#else
-		return 0;
 #endif
-	};
 
+	if (best_dt_name == NULL) {
+		/* No DT name was provided, so accept the first config. */
+		return 0;
+	}
 #ifdef CONFIG_PINE64_DT_SELECTION
 /* Differentiate the two Pine64 board DTs by their DRAM size. */
 	if (strstr(name, "-pine64") && strstr(best_dt_name, "-pine64")) {
-- 
2.26.2

